//
//  CVcell.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVcell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *CollectionImageView;
@property (strong, nonatomic) IBOutlet UILabel *CollectionImageLbl;

@end
