//
//  DashBoardViewController.m
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import "DashBoardViewController.h"

@interface DashBoardViewController ()

@end
NSArray *imagearr,*detailsarr;
@implementation DashBoardViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"DashBoard";
    self.navigationController.navigationBar.topItem.title=@"Back";
    //imagearr=[[NSArray alloc]initWithObjects:@"1",@"1",@"1",@"1",@"1"
    _RemLbl.text=@"Reminders";
    _CreateRemLbl.text=@"Create Reminder";
    _AccSettLbl.text=@"Account Settings";
    _RemSettLbl.text=@"Reminder Settings";
    _LogOutLbl.text=@"Logout";
    
    
#pragma Designing Button on imageview
    
    _RemSettBtnOutlet.layer.borderColor=[UIColor blueColor].CGColor;
    _RemSettBtnOutlet.layer.borderWidth=1.0f;
    
    
    _CreateRemBtnOutlet.layer.borderColor=[UIColor blueColor].CGColor;
    _CreateRemBtnOutlet.layer.borderWidth=1.0f;

    
    _AccSettBtnOutlet.layer.borderColor=[UIColor blueColor].CGColor;
    _AccSettBtnOutlet.layer.borderWidth=1.0f;

    
    
    _RemViewBtnOutlet.layer.borderColor=[UIColor blueColor].CGColor;
    _RemViewBtnOutlet.layer.borderWidth=1.0f;

    
    
    _LogoutBtnOutlet.layer.borderColor=[UIColor blueColor].CGColor;
    _LogoutBtnOutlet.layer.borderWidth=1.0f;

    
    
    
    // Do any additional setup after loading the view.
    // hi
}

-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"DashBoard";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)RemBtn:(id)sender
{
    
    [self performSegueWithIdentifier:@"Reminder" sender:self];
    
    
}
- (IBAction)CreateRemBtn:(id)sender
{
    
    
    // firstly searches in contact list

    
    [self performSegueWithIdentifier:@"CreateRem" sender:self];





}
- (IBAction)AccSettBtn:(id)sender
{

[self performSegueWithIdentifier:@"AccSettings" sender:self];

}
- (IBAction)RemSettBtn:(id)sender
{
[self performSegueWithIdentifier:@"RemSettings" sender:self];
}
- (IBAction)LogOutBtn:(id)sender
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure to Logout???" delegate:self cancelButtonTitle:@"Cancel"otherButtonTitles:@"Ok", nil];
    [alert show];


}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 if(buttonIndex==1)
 {
 [self performSegueWithIdentifier:@"Logout" sender:self];
 }
}
@end
