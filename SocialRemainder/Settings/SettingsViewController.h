//
//  SettingsViewController.h
//  SocialRemainder
//
//  Created by Samprita Roy on 22/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *ProfileLbl;
@property (strong, nonatomic) IBOutlet UITextField *OldPwdTxt;
@property (strong, nonatomic) IBOutlet UITextField *NewPwdTxt;
@property (strong, nonatomic) IBOutlet UITextField *ConfirmPwdTxt;
@property (strong, nonatomic) IBOutlet UIButton *ResetBtn;
@property (strong, nonatomic) IBOutlet UISwitch *NotificationBtn;
@property (strong, nonatomic) IBOutlet UILabel *NotificationLbl;
@property (strong, nonatomic) IBOutlet UIButton *UpdateSettings;
- (IBAction)ResetBtn:(id)sender;
- (IBAction)UpdateSettings:(id)sender;
- (IBAction)NotificationBtn:(id)sender;

@end
