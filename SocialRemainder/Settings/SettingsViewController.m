//
//  SettingsViewController.m
//  SocialRemainder
//
//  Created by Samprita Roy on 22/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"Setting";
    self.navigationController.navigationBar.topItem.title=@"Back";
    self.OldPwdTxt.delegate=self;
    self.NewPwdTxt.delegate=self;
    self.ConfirmPwdTxt.delegate=self;
    [_OldPwdTxt becomeFirstResponder];
    
    // Do any additional setup after loading the view.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Setting";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ResetBtn:(id)sender {
}

- (IBAction)UpdateSettings:(id)sender {
}

- (IBAction)NotificationBtn:(id)sender {
}
@end
