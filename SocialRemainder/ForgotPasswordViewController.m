//
//  ForgotPasswordViewController.m
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "NSString+StringValidation.h"
#import "AFNetworking.h"
//http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/Recovery
NSString *const BaseUrl2=@"http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/";
@interface ForgotPasswordViewController ()

@end
NSString *ForgotEmail;
int tag5,WebResponse;
@implementation ForgotPasswordViewController
@synthesize ForgotPasslbl,txtForgotEmail;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title=@"Forgot Password";
    self.navigationController.navigationBar.topItem.title=@"Back";
    ForgotPasslbl.text=@"Enter the Email ID you register with to retrive your password there.";
    self.txtForgotEmail.delegate=self;
    [txtForgotEmail becomeFirstResponder];
   // [self getUUID];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Forgot Password";
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//hello
//UserRecoveryForm[login_or_email]


-(void)WebServiceCall
{
    NSString *stringUrl=[NSString stringWithFormat:@"%@Recovery",BaseUrl2];
    
    AFHTTPRequestOperationManager *manager =[AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"UserRecoveryForm[login_or_email]":txtForgotEmail.text};
    
    
    [manager POST:stringUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary *dict=[responseObject objectForKey:@"result"];
        NSLog(@"dict: %@", dict);
        NSDictionary *dict1=[dict objectForKey:@"status"];
        NSLog(@"dict1: %@", dict1);
        ForgotEmail=[dict1 objectForKey:@"message"];
        NSLog(@"Mess: %@", ForgotEmail);
        
        tag5=1;  //Setting tag to check wheather WebService is Called or not...
        
        //After Calling webservice call same button Action from where we call webservice.
        [_ForgotPassBtn setHighlighted:YES];
        [_ForgotPassBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
        [_ForgotPassBtn performSelector:@selector(setHighlighted:) withObject:NO afterDelay:0.10];

    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:[error localizedDescription]delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma Checking Validation And Creating Alert On WebService Responses.
- (IBAction)btnForgotPass:(id)sender
{
    if(txtForgotEmail.text<=0)
    {
        UIAlertView *alertFg=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Enter Registered Email Id...." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertFg show];
    }
    else
    {
        if(tag5==1)   //Checking WebService Response And Creating Alert..
        {
            if(![ForgotEmail isEqualToString:@""])
            {
                tag5=0;  //Setting For Calling WebService Everytime of Execution..
                if([ForgotEmail isEqualToString:@"Email is incorrect."])
                {
                    UIAlertView *ForgotAlert=[[UIAlertView alloc]initWithTitle:@"Alert" message:[NSString stringWithFormat:@"%@ or not registered.",ForgotEmail] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [ForgotAlert show];
                    txtForgotEmail.text=@"";
                    [txtForgotEmail becomeFirstResponder];
                }
                else
                {
                    UIAlertView *ForgotAlert=[[UIAlertView alloc]initWithTitle:@"Alert" message:[NSString stringWithFormat:@"%@     \n Check Your Registered EmailId.",ForgotEmail] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [ForgotAlert show];
                    txtForgotEmail.text=@"";
                    [txtForgotEmail becomeFirstResponder];

                  //Further Code If required....
                }
            }
        }
        else
        {
            [self WebServiceCall];
        }
    }
}
@end
