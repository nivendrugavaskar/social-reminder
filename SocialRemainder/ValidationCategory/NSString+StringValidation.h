//
//  NSString+StringValidation.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringValidation)

+(BOOL)validateEmail:(NSString *)email;
+(BOOL)validatePassword:(NSString *)pass;
@end
