//
//  NSString+StringValidation.m
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import "NSString+StringValidation.h"

@implementation NSString (StringValidation)
+ (BOOL)validateEmail:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
+ (BOOL)validatePassword:(NSString *)pass
{
    NSString *pwdchk=@"^[a-z]{1,10}[._%/&#$*+@]{1,2}[0-9]{1,5}$";
    NSPredicate *pwdTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pwdchk];
    return [pwdTest evaluateWithObject:pass];
}
@end
