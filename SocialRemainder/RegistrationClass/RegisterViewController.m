//
//  RegisterViewController.m
//  SocialRemainder
//
//  Created by Samprita Roy on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
//#import "ModelClass.h"
NSString *const BaseUrl1=@"http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/";


 //NSString *const BaseUrl1=@"http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/Registration";



@interface RegisterViewController ()

@end
//ModelClass * model;
int tag1=0,tag4;  //For Checking WebService Response.
NSString *alert1;
@implementation RegisterViewController
@synthesize txtconpasswrd,txtemail,txtname,txtpassword,devicestring;

- (void)viewDidLoad
{
    [super viewDidLoad];
  //model=[[ModelClass alloc]init];
    
    self.navigationItem.title = @"Registration";
    self.navigationController.navigationBar.topItem.title=@"Back";
    self.txtconpasswrd.delegate=self;
    self.txtemail.delegate=self;
    self.txtname.delegate=self;
    self.txtpassword.delegate=self;
    //_devicestring=[NSString stringWithFormat:@"device id",CFStringRef];
    
    [txtname becomeFirstResponder];
    [self getDeviceId];
    
    //[self WebServiceCall];
    // Do any additional setup after loading the view.
}
//- (NSString *)getUUID {
//    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceUUID"];
//    if (string == nil) {
//        CFUUIDRef   uuid;
//        CFStringRef uuidStr;
//        
//        uuid = CFUUIDCreate(NULL);
//        uuidStr = CFUUIDCreateString(NULL, uuid);
//        
//        string = [NSString stringWithFormat:@"%@", uuidStr];
//        NSLog(@"Device id is:...%@",uuidStr);
//        
//        
//        
//        [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"deviceUUID"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        
////        CFRelease(uuidStr);
////        CFRelease(uuid);
//    }
//    
//    return string;
//}

// Please dont delete this function: below
# pragma device token:


- (NSString *) getDeviceId {
    NSString *deviceID;
    UIDevice *device = [UIDevice currentDevice];
    if ([UIDevice instancesRespondToSelector:@selector(identifierForVendor)]) {
        deviceID = [[device identifierForVendor] UUIDString];
    } else {
        // In iOS5.x or older 'identifierForVendor' is not available
        deviceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"app identifier"];
        if (!deviceID) {
            CFUUIDRef uuidRef = CFUUIDCreate(NULL);
            CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
            deviceID = [NSString stringWithString:(NSString *) CFBridgingRelease(uuidStringRef)];
            CFRelease(uuidRef);
             NSLog(@"Device id is:...%@",deviceID);
            [[NSUserDefaults standardUserDefaults] setObject:deviceID forKey:@"app identifier"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    return deviceID;
}
#pragma WEB SERVICE 
//Profile[name], Profile[device_token], Profile[token_type]

-(void)WebServiceCall
{
    NSString *stringUrl=[NSString stringWithFormat:@"%@Registration",BaseUrl1];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"User[email]": txtemail.text,@"User[password]":txtpassword.text,@"Profile[name]":txtname.text,@"Profile[device_token]":@"",@"Profile[token_type]":@"iOS"};
    

    [manager POST:stringUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary *dict=[responseObject objectForKey:@"result"];
        NSDictionary *dict2=[dict objectForKey:@"status"];
        alert1=[dict2 objectForKey:@"message"];
        
          //Setting for Alert....
      /*  NSString *strsuccess=[NSString stringWithFormat:@"%@",[parameters objectForKey:@"status"]];
        if ([strsuccess isEqualToString:@""])
        {
            NSDictionary *dict1=[NSDictionary alloc];
            
            dict1=[parameters objectForKey:@"response"];
            NSLog(@"Value.......%@",dict1);
            
        }*/
        tag4=1;//Setting tag to check wheather WebService is Called or not...
        
        //After Calling webservice call same button Action from where we call webservice.
        [_btnreg setHighlighted:YES];
        [_btnreg sendActionsForControlEvents:UIControlEventTouchUpInside];
        [_btnreg performSelector:@selector(setHighlighted:) withObject:NO afterDelay:0.12];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {tag1=1;
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:[error localizedDescription]delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];

}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Registration";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_scrollReg setContentOffset:rt animated:YES];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rc=[textField bounds];
    rc=[textField convertRect:rc toView:_scrollReg];
    CGPoint pt;
    pt=rc.origin;
    pt.x=0;
    pt.y-=50;
    [_scrollReg setContentOffset:pt animated:YES];
}
- (IBAction)btnregisterclick:(id)sender
{
    [_scrollReg setContentOffset:rt animated:YES];
    //[self.view endEditing:YES];
    
#pragma Validation Checking On button Click ---------
    
    if(txtname.text.length<=0 || txtemail.text.length<=0 || txtpassword.text.length<=0 || txtconpasswrd.text.length<=0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Fill All Required Fields..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [txtname becomeFirstResponder];
    }
    #pragma Email validation Checking through Catagory Class--------
    else if (![NSString validateEmail:[txtemail text]])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter Proper Email Id..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        txtemail.text=@"";
        [txtemail becomeFirstResponder];
    }
#pragma Password validation Checking through Catagory Class--------
    else if (txtpassword.text.length<6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Password Must Contain Six Characters..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        txtpassword.text=@"";
        [txtpassword becomeFirstResponder];
    }

       else if (![txtpassword.text isEqualToString: txtconpasswrd.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Password And Confirm Password Are Not Same...." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
       
        txtconpasswrd.text=@"";
        [txtconpasswrd becomeFirstResponder];
         [alert show];
    }
    //After Check All Validation insert data into table..
    else if(txtname.text.length>0 || txtemail.text.length>0 || txtpassword.text.length>0 || txtconpasswrd.text.length>0)
    {
        
        if(tag4==1)    //Checking WebService Response And Creating Alert..
        {
            if(![alert1 isEqualToString:@""])
            {
                UIAlertView *regalert=[[UIAlertView alloc]initWithTitle:@"Error" message:alert1 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [regalert show];
            tag4=0;  //Setting For Calling WebService Everytime of Execution..
                if([alert1 isEqualToString:@"This user's email address already exists."])
                {
                txtemail.text=@"";
                [txtemail becomeFirstResponder];
                }
            }
            else
            {
                if(tag1==1)
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Infomation" message:@"Registration Not Successful. \n        Try Again..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Infomation" message:@"Registration Successful." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    //alert.tag=100;
                    
                    
                    txtemail.text=@"";
                    txtconpasswrd.text=@"";
                    txtname.text=@"";
                    txtpassword.text=@"";
                    [alert show];
                    
                }
                //[self performSegueWithIdentifier:@"Reg2dash" sender:self];
            }
        }
        else
        {
        [self WebServiceCall];
        }
       
        
        
    }
    
  
       // [self performSegueWithIdentifier:@"gotoLogin" sender:self];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    // Is this my Alert View?
    if (alertView.tag == 100) {
        //Yes
        
        // You need to compare 'buttonIndex' & 0 to other value(1,2,3) if u have more buttons.
        // Then u can check which button was pressed.
        if (buttonIndex == 0)
        {// 1st Other Button
            
            
            [self performSegueWithIdentifier:@"Reg2dash" sender:self];
            
        }
        else if (buttonIndex == 1)
        {// 2nd Other Button
            
            
        }
        
    }
    else {
        //No
        // Other Alert View
        
    }
    
}

@end
