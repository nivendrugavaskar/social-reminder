//
//  RegisterViewController.h
//  SocialRemainder
//
//  Created by Samprita Roy on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
@interface RegisterViewController : UIViewController<UITextFieldDelegate>
{
    CGPoint rt;
}



@property(strong,nonatomic)IBOutlet UITextField *txtname;
@property(strong,nonatomic)IBOutlet UITextField *txtemail;
@property(strong,nonatomic)IBOutlet UITextField *txtpassword;
@property(strong,nonatomic)IBOutlet UITextField *txtconpasswrd;
@property(strong,nonatomic)NSString *devicestring;




@property (weak, nonatomic) IBOutlet UIButton *btnreg;

@property(strong,nonatomic)IBOutlet UIImageView *imgRegBackVw;
@property(strong,nonatomic)IBOutlet UIScrollView *scrollReg;



- (IBAction)btnregisterclick:(id)sender;
@end
