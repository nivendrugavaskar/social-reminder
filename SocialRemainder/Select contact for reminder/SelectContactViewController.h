//
//  SelectContactViewController.h
//  SocialRemainder
//
//  Created by Samprita Roy on 18/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface SelectContactViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,ABPeoplePickerNavigationControllerDelegate,UISearchDisplayDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *mSearchBar;// To search contacts from Contact list

@property (strong, nonatomic)NSMutableArray *contactList;//To hold data of contact list ...Original array holding data

@property (strong, nonatomic) IBOutlet UITableView *tblsearch;
@property (nonatomic, strong) NSMutableArray *tableData;

@property(strong,nonatomic)NSMutableArray *searchedDataArray;//Array holding searched data...SearchedArray

@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;
@property (nonatomic, retain) UISearchDisplayController	*searchDisplayController;

@end
