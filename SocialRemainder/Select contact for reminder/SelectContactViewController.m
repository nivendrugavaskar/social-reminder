//
//  SelectContactViewController.m
//  SocialRemainder
//
//  Created by Samprita Roy on 18/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import "SelectContactViewController.h"
#import "ContactlistTableViewCell.h"
#import "Person.h"
#import "CreateRemViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface SelectContactViewController ()
//@property (nonatomic, strong) NSMutableArray *arrContactsData;


@end
NSString *name,*phoneno;
@implementation SelectContactViewController


- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = @"Select Contact For Reminder";
    self.navigationController.navigationBar.topItem.title=@"Back";
    self.tableData = [[NSMutableArray alloc] init];
    self.tblsearch.delegate=self;
//    [self loadData];
    [_tblsearch reloadData];
    self.mSearchBar.delegate=self;
   [self getPersonOutOfAddressBook];
    
    
#pragma  searching delegate
    
    [_mSearchBar setDelegate:self];
    
//    [self.searchDisplayController setSearchResultsDataSource:self];
//    [self.searchDisplayController setSearchResultsDelegate:self];
//    [self.searchDisplayController setDelegate:self];
//    
//    [self.tblsearch setTableHeaderView:self.searchDisplayController.searchBar];

    // Do any additional setup after loading the view.
}


//-(void) loadData
//{
//    [_tblsearch reloadData];
//    
//}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Select Contact For Reminder";
}




-(void)showAddressBook
{
    _addressBookController = [[ABPeoplePickerNavigationController alloc] init];
    [_addressBookController setPeoplePickerDelegate:self];
    [self presentViewController:_addressBookController animated:YES completion:nil];
}
-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [_addressBookController dismissViewControllerAnimated:YES completion:nil];
}

/*-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    
    NSMutableDictionary *contactInfoDict = [[NSMutableDictionary alloc]
                                            initWithObjects:@[@"", @""]
                                            forKeys:@[@"firstName", @"mobileNumber"]];
    
    CFTypeRef generalCFObject;
    generalCFObject = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    if (generalCFObject)
    {
        [contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"firstName"];
        CFRelease(generalCFObject);
        
    }
    ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef all = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex n = ABAddressBookGetPersonCount(addressBook);
    
    for( int i = 0 ; i < n ; i++ )
    {
        ABRecordRef ref = CFArrayGetValueAtIndex(all, i);
        NSString *firstName = (__bridge NSString *)ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        NSLog(@"Name %@", firstName);
        
        ABMultiValueRef *phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
        {
            NSString *phoneLabel = @""; // ???
            
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
            //CFRelease(phones);
            NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
            CFRelease(phoneNumberRef);
            NSLog(@"  - %@ (%@)", phoneNumber, phoneLabel);
            
        
        }
    }
        
        
        if (ABPersonHasImageData(person)) {
            NSData *contactImageData = (__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail);
            
            [contactInfoDict setObject:contactImageData forKey:@"image"];
        }
        
        
        if (_arrContactsData == nil)
        {
            _arrContactsData = [[NSMutableArray alloc] init];
        }
        [_arrContactsData addObject:contactInfoDict];
        
        [self.tblsearch reloadData];
        
        [_addressBookController dismissViewControllerAnimated:YES completion:nil];
        
        NSLog(@"Data at index 0 is %@",_arrContactsData[0]);
        
        //hi
        
         return NO;
   }*/
- (void)getPersonOutOfAddressBook
{
        ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        //dispatch_release(semaphore);
    }
    
    else { // We are on iOS 5 or Older
        accessGranted = YES;
        [self listPeopleInAddressBook:addressBook];
    }
    
    if (accessGranted) {
       [self listPeopleInAddressBook:addressBook];
        
    }
    
}

- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook
{
    
    
    _contactList = [[NSMutableArray alloc] init];
   // CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
   // CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSInteger numberOfPeople = [allPeople count];
    NSLog(@"%@",allPeople);
    
    
    for (NSInteger i = 0; i < numberOfPeople; i++)
    
    {
       
        NSMutableDictionary *dOfPerson=[NSMutableDictionary dictionary];
        
        //for name in contact list
        ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        NSLog(@"Name:%@ %@", firstName, lastName);
        
        
        //Set Dictionary for the contact List
        
         [dOfPerson setObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"name"];
        
        //For Phonenumber in Contact List
        
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex numberOfPhoneNumbers = ABMultiValueGetCount(phoneNumbers);
        for (CFIndex i = 0; i < numberOfPhoneNumbers; i++)
        {
            NSString *phoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phoneNumbers, i));
            [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, i) forKey:@"Phone"];
            NSLog(@"  phone:%@", phoneNumber);
        }
        
        CFRelease(phoneNumbers);
          NSLog(@"dic value is %@",dOfPerson);
        
        
        //For Image in Contact list
        
        
        if (ABPersonHasImageData(person)) {
            NSData *contactImageData = (__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail);
            //NSData  *imgData = (NSData *)ABPersonCopyImageData(person);
            
            UIImage  *img = [UIImage imageWithData:contactImageData];
            
            NSLog(@"Image %@",img);
            [dOfPerson setObject:contactImageData forKey:@"image"];
        }
        
        
        NSLog(@"=============================================");
        [_contactList addObject:dOfPerson]; //Adding Dic to Array
        
    }
      NSLog(@"Contacts = %@",_contactList);
  
    
}



- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person NS_AVAILABLE_IOS(8_0)

{
}



#pragma Tableview Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_contactList count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    static NSString *cellid=@"ContactlistView";
    ContactlistTableViewCell *cell = (ContactlistTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    
//    // Configure Cell
    //cell.SideVw.layer.borderColor=[UIColor blackColor].CGColor;
  //  cell.SideVw.layer.borderWidth=1.0f;
//    
//    
//    cell.lblname.text=@"Abhi";
//    cell.lblphno.text=@"9812875655";
//    cell.imgcontactlist.image=[UIImage imageNamed:@""];
//    cell.imgcontactlist.layer.borderColor=[UIColor blueColor].CGColor;
//    cell.imgcontactlist.layer.borderWidth=1.0f;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if([_contactList count] > 0)
    {
        
       cell.lblname.text=[NSString stringWithFormat:@"%@",[[_contactList objectAtIndex:indexPath.row] valueForKey:@"name"]];
        
        cell.lblphno.text=[NSString stringWithFormat:@"%@",[[_contactList objectAtIndex:indexPath.row] valueForKey:@"Phone"]];
       
    }
  
    return cell;
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
       return 70.0f;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    name=[NSString stringWithFormat:@"%@",[[_contactList objectAtIndex:indexPath.row] valueForKey:@"name"]];
       [self performSegueWithIdentifier:@"SelectContact" sender:self];
    
    phoneno=[NSString stringWithFormat:@"%@",[[_contactList objectAtIndex:indexPath.row]valueForKey:@"Phone"]];
    [self performSegueWithIdentifier:@"SelectContact" sender:self];
    
    
    
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CreateRemViewController *seg=[segue destinationViewController];
  if([[segue identifier]isEqualToString:@"SelectContact"])
  {
      seg.strlblnameContact=name;
      seg.strlblphoneContact=phoneno;
      
  }
}
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

#pragma Goto Create Reminder Page via segue "Create Rem"
    // [self performSegueWithIdentifier:<#(nonnull NSString *)#> sender:<#(nullable id)#>]
    
    //ContactViewController *contactViewController = [[ContactlistTableViewCell alloc] initWithPerson:person];
   // [self.navigationController pushViewController:contactViewController animated:YES];




#pragma Search implementing


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.searchedDataArray removeAllObjects];
    for (Person *person in _contactList)
    {
        if ([scope isEqualToString:@"All"] || [person.firstName isEqualToString:scope])
        {
            NSComparisonResult result = [person.firstName compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame)
            {
                [self.searchedDataArray addObject:person];
            }
        }
    }
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.mSearchBar setShowsCancelButton:YES animated:YES];
}


- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.mSearchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0) {
        //searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"Search bar tapped");
        
        
        //NSString* filter = @"%K CONTAINS %@";
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@" , searchText];
        
         //NSPredicate* predicate = [NSPredicate predicateWithFormat:filter, @"SELF", searchText  ];
     //   NSPredicate *predicate =[NSPredicate predicateWithFormat: @"SELF CONTAINS[cd] %@", searchText];
        NSArray *arr= [[_contactList filteredArrayUsingPredicate:predicate] mutableCopy];
       
        _contactList=[arr mutableCopy];
        
        
        [_tblsearch reloadData];
        
        
    }
    else
    {
        _searchedDataArray=_contactList;
        [_tblsearch reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.mSearchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.mSearchBar resignFirstResponder];
    _searchedDataArray=_contactList;
    
    [self.tblsearch reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
