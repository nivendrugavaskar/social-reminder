//
//  ContactlistTableViewCell.h
//  SocialRemainder
//
//  Created by Samprita Roy on 18/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>

@interface ContactlistTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imgcontactlist;
@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet UILabel *lblphno;
@property(strong,nonatomic)NSMutableArray *arrimg;
@property (strong, nonatomic) IBOutlet UIView *SideVw;


- (IBAction)GotoCreateRemPage:(id)sender;
@property(strong,nonatomic)IBOutlet UIButton *btnGotoReminder;


@end
