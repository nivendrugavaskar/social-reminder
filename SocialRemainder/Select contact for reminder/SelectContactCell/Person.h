//
//  Person.h
//  SocialRemainder
//
//  Created by Samprita Roy on 23/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong) NSString *firstName;

@property (nonatomic, strong) NSString *phoneno;
@property (nonatomic, strong) NSString *imgname;
//+ (id)newCloud:(NSString *)name withClassification:(NSString *)phone withInformation:(NSString *)imagename withImage:(NSString *)imageFile;

//@property (nonatomic, strong) NSString *lastName;
//@property (nonatomic, strong) NSString *fullName;
//@property (nonatomic, strong) NSString *homeEmail;
//@property (nonatomic, strong) NSString *workEmail;

@end
