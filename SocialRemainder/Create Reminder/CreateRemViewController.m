//
//  CreateRemViewController.m
//  SocialRemainder
//
//  Created by Samprita Roy on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import "CreateRemViewController.h"
#import "SelectContactViewController.h"

@interface CreateRemViewController ()
-(void)populateContactData;

@end
int tag;
NSMutableArray *RemDescArr,*RemOption,*RemCelebArr;
@implementation CreateRemViewController
@synthesize strlblnameContact;
- (void)viewDidLoad {
    [super viewDidLoad];
        self.navigationItem.title = @"Create Reminder";
    self.navigationController.navigationBar.topItem.title=@"Back";
    [self backGroundDesign];
    RemDescArr=[[NSMutableArray alloc]initWithObjects:@"BirthDay",@"Aniversary",@"ExamDay", nil];
    RemOption=[[NSMutableArray alloc]initWithObjects:@"BirthDay",@"Aniversary",@"ExamDay", nil];
    RemCelebArr=[[NSMutableArray alloc]initWithObjects:@"BirthDay",@"Aniversary",@"ExamDay", nil];
    
    _lblshwcontct.text=strlblnameContact;
    
#pragma String to hold value of contact number
    
    // _strlblphoneContact
    _CelebrateTxt.delegate=self;
    _RemOptionTxt.delegate=self;
    _RemDateTxt.delegate=self;
    
    
    //[self populateContactData];
    // Do any additional setup after loading the view.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title = @"Create Reminder";
}
-(void)populateContactData
{
    NSString *contactFullName = [NSString stringWithFormat:@"%@" ,[_dictContactDetails objectForKey:@"firstName"]];
    
    [_lblshwcontct setText:contactFullName];
}

#pragma BackgroundLayouts For UI.....
-(void)backGroundDesign
{
    self.bckVw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.bckVw.layer.borderWidth=1.0f;
    self.fifthvw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.fifthvw.layer.borderWidth=1.0f;
    self.firstvw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.firstvw.layer.borderWidth=1.0f;
    self.secondvw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.secondvw.layer.borderWidth=1.0f;
    self.thirdvw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.thirdvw.layer.borderWidth=1.0f;
    self.fourthvw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.fourthvw.layer.borderWidth=1.0f;
    _btnremdrop.layer.borderColor=[UIColor blackColor].CGColor;
    _btnremdrop.layer.borderWidth=1.0f;
    _btnremthird.layer.borderColor=[UIColor blackColor].CGColor;
    _btnremthird.layer.borderWidth=1.0f;
    _btnfourthdrop.layer.borderWidth=1.0f;
    _btnfourthdrop.layer.borderColor=[UIColor blackColor].CGColor;
    _selectfrmbtn.layer.borderWidth=1.0f;
    _selectfrmbtn.layer.borderColor=[UIColor blackColor].CGColor;
    [[self.selectfrmbtn layer]setCornerRadius:4.0f];
    _btnsave.layer.borderWidth=1.0f;
    _btnsave.layer.borderColor=[UIColor blackColor].CGColor;
    [[self.btnsave layer]setCornerRadius:4.0f];
    [[self.remdesc layer]setBorderColor:[[UIColor blackColor]CGColor]];
    [[self.remdesc layer]setBorderWidth:1.0];
    
    
    
    
    //[[self.remdesc layer] setCornerRadius:2];
    
    //    [[self.textview layer] setBorderColor:[[UIColor grayColor] CGColor]];
    //    [[self.textview layer] setBorderWidth:2.3];
    //    [[self.textview layer] setCornerRadius:15];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btndropdown:(id)sender
{
   //int i;
    float width,height,X,Y;
    X=self.RemDateTxt.frame.origin.x;
    Y=self.RemDateTxt.frame.origin.y;
    width=self.RemDateTxt.frame.size.width+30;
    height=self.RemDateTxt.frame.size.height+50;
    
        [picker1 setCenter:CGPointMake(221, 210)];
        
        [UIPickerView beginAnimations:nil context:nil];
        [UIPickerView setAnimationDuration:2.0f];
        picker1=[[UIDatePicker alloc]initWithFrame:CGRectMake(X, 160, width, height)];
        
        
        picker1.backgroundColor = [UIColor clearColor];
        picker1.layer.borderWidth=2.0f;
        picker1.layer.borderColor=[[UIColor blackColor]CGColor];
        
        picker1.datePickerMode=UIDatePickerModeDate;
        [picker1 setMinimumDate:[NSDate date]];
        
        [picker1 addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventAllEvents];
        format=[[NSDateFormatter alloc]init];
       //YYYY-MM-DD (2015-10-01) Date Format
    
    
        [format setDateFormat:@"YYYY-MM-DD"];
        _RemDateTxt.text=[format stringFromDate:picker1.date];
        [self.view addSubview:picker1];
        [UIPickerView commitAnimations];
    
    
}
-(void)updateTextField:(id)sender
{
    self.RemDateTxt.text = [format stringFromDate:picker1.date];
    picker1.hidden=TRUE;
}
- (IBAction)btnSelectfrm:(id)sender
{
    tag=1;
    float width,height,X,Y;
    width=self.view.frame.size.width;
    height=self.view.frame.size.height;
    X=self.view.frame.origin.x;
    Y=self.view.frame.origin.y;
    picker=[[UIPickerView alloc]initWithFrame:CGRectMake(width/4,320, width/2, height/3)];
    picker.delegate=self;
    picker.dataSource=self;
    picker.showsSelectionIndicator=YES;
    
    _remdesc.text=[RemDescArr objectAtIndex:0];
    picker.backgroundColor=[UIColor clearColor];
    [self.view addSubview:picker];
    
}

- (IBAction)btnclickRemOption:(id)sender
{
    tag=2;
    float width,height,X,Y;
    width=self.view.frame.size.width;
    height=self.view.frame.size.height;
    X=self.view.frame.origin.x;
    Y=self.view.frame.origin.y;
    pickOptn=[[UIPickerView alloc]initWithFrame:CGRectMake(width/4,320, width/2, height/3)];
    pickOptn.delegate=self;
    pickOptn.dataSource=self;
    pickOptn.showsSelectionIndicator=YES;
    
    _RemOptionTxt.text=[RemOption objectAtIndex:0];
    pickOptn.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:pickOptn];
    
}
- (IBAction)btnthirdhow:(id)sender
{
    tag=3;
    float width,height,X,Y;
    width=self.view.frame.size.width;
    height=self.view.frame.size.height;
    X=self.view.frame.origin.x;
    Y=self.view.frame.origin.y;
    pickCeleb=[[UIPickerView alloc]initWithFrame:CGRectMake(width/4,320, width/2, height/3)];
    pickCeleb.delegate=self;
    pickCeleb.dataSource=self;
    pickCeleb.showsSelectionIndicator=YES;
    
    _CelebrateTxt.text=[RemCelebArr objectAtIndex:0];
    pickCeleb.backgroundColor=[UIColor clearColor];
    [self.view addSubview:pickCeleb];
}
- (IBAction)btnsaveClick:(id)sender {
    
    //Insert in database
    
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(tag==1)
    {
    return RemDescArr.count;
    }
    else if (tag==2)
    {
       return RemOption.count;
    }
    else
    {
        return RemCelebArr.count;
    }
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(tag==1)
    {
    return RemDescArr[row];
    }
    else if (tag==2)
    {
        return RemOption[row];
    }
    else
    {
        return RemCelebArr[row];
    }
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(tag==1)
    {
    _remdesc.text=[RemDescArr objectAtIndex:row];
        picker.hidden=TRUE;
    }
    else if (tag==2)
    {
        _RemOptionTxt.text=[RemOption objectAtIndex:row];
        pickOptn.hidden=TRUE;
    }
    else
    {
        _CelebrateTxt.text=[RemCelebArr objectAtIndex:row];
        pickCeleb.hidden=TRUE;
    }
}
@end
