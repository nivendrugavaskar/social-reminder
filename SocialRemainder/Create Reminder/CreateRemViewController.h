//
//  CreateRemViewController.h
//  SocialRemainder
//
//  Created by Samprita Roy on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateRemViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    IBOutlet UIDatePicker *picker1;
    UIPickerView *picker,*pickOptn,*pickCeleb;
    NSDateFormatter *format;
}



@property (nonatomic, strong) NSDictionary *dictContactDetails; //Dic to hold contact name coming from Contact list of Device
#pragma FirstView Outlets
@property (weak, nonatomic) IBOutlet UILabel *lblcntct;
@property (weak, nonatomic) IBOutlet UILabel *lblshwcontct; //Displays name of contact selected from contact list
@property (weak, nonatomic) IBOutlet UILabel *lblrem;
@property(strong,nonatomic)NSString *strlblnameContact;
@property(strong,nonatomic)NSString *strlblphoneContact;



@property (strong, nonatomic) IBOutlet UITextField *RemDateTxt;
@property (weak, nonatomic) IBOutlet UIView *firstvw;




#pragma BackView Design
@property (strong, nonatomic) IBOutlet UIView *bckVw;
@property(strong,nonatomic)IBOutlet UIImageView *imgbackVw;



#pragma View separation Line
@property (strong,nonatomic)IBOutlet UIView *frsVwSep;
@property (strong,nonatomic)IBOutlet UIView *secndVwSep;
@property (strong,nonatomic)IBOutlet UIView *thirdVwSep;
@property (strong,nonatomic)IBOutlet UIView *fourthVwSep;


#pragma SecondView Outlets
@property (weak, nonatomic) IBOutlet UIButton *btnremdrop; //1 Reminder Date

@property (weak, nonatomic) IBOutlet UIView *secondvw;
@property (weak, nonatomic) IBOutlet UIView *thirdvw;
@property (strong, nonatomic) IBOutlet UITextField *RemOptionTxt;
@property (strong, nonatomic) IBOutlet UITextField *CelebrateTxt;

@property (weak, nonatomic) IBOutlet UIButton *btnremthird;// 2 ...Reminder Option
- (IBAction)btnSelectfrm:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *selectfrmbtn;
@property (weak, nonatomic) IBOutlet UITextView *remdesc;



- (IBAction)btnclickRemOption:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *fourthvw;
@property (weak, nonatomic) IBOutlet UIButton *btnfourthdrop;//3 How celebrate

- (IBAction)btnthirdhow:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnsave;
- (IBAction)btnsaveClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *fifthvw;
@end
