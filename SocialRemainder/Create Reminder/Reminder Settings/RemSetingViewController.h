//
//  RemSetingViewController.h
//  SocialRemainder
//
//  Created by Samprita Roy on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemSetingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblRemsettings;

@end
