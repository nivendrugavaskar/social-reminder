//
//  RemsettingTableViewCell.h
//  SocialRemainder
//
//  Created by Samprita Roy on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemsettingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblremdate;
@property (weak, nonatomic) IBOutlet UILabel *lbltotalRem;
@property (weak, nonatomic) IBOutlet UILabel *lblRemtime;
@property (weak, nonatomic) IBOutlet UILabel *lblShowremdate;
@property (weak, nonatomic) IBOutlet UILabel *lblshwTotalRem;
@property (weak, nonatomic) IBOutlet UILabel *lblShwRemTime;
@property (strong, nonatomic) IBOutlet UIView *SideView;
@end
