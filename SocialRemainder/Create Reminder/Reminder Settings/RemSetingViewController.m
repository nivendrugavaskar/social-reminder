//
//  RemSetingViewController.m
//  SocialRemainder
//
//  Created by Samprita Roy on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import "RemSetingViewController.h"
#import "RemSettingCell/RemsettingTableViewCell.h"
@interface RemSetingViewController ()

@end
NSMutableArray *dataArr;
@implementation RemSetingViewController
@synthesize tblRemsettings;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"Reminder Settings";
    self.navigationController.navigationBar.topItem.title=@"Back";
    self.tblRemsettings.separatorColor=[UIColor clearColor];
    self.tblRemsettings.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    dataArr=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4", nil];

    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Reminder Settings";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArr count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"Remcell";
    RemsettingTableViewCell *cell = (RemsettingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    
    // Configure Cell
    cell.SideView.layer.borderWidth=1.0f;
    cell.SideView.layer.borderColor=[UIColor blackColor].CGColor;
    cell.lblShowremdate.text=@"1";
    cell.lblShwRemTime.text=@"2";
    cell.lblshwTotalRem.text=@"3";
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
