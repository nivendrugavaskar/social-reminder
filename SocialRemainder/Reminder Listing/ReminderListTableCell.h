//
//  ReminderListTableCell.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReminderListTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *RemListNmLbl;
@property (strong, nonatomic) IBOutlet UILabel *RemListDtlbl;
@property (strong, nonatomic) IBOutlet UILabel *RemListReasonLbl;
@property (strong, nonatomic) IBOutlet UIImageView *RemListImgVw;
@property (strong, nonatomic) IBOutlet UIButton *RemListBtn;


@end
