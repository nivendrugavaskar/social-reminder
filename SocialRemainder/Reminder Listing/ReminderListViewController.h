//
//  ReminderViewController.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReminderListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *RemlistTbl;
@property (strong, nonatomic) IBOutlet UIView *ListVw;
@property (strong, nonatomic) IBOutlet UIView *AdView;

@end
