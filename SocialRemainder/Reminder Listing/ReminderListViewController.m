//
//  ReminderViewController.m
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 21/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import "ReminderListViewController.h"
#import "ReminderListTableCell.h"
@interface ReminderListViewController ()

@end

@implementation ReminderListViewController
@synthesize RemlistTbl;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"Reminder Listing";
    self.navigationController.navigationBar.topItem.title=@"Back";
    _ListVw.layer.borderWidth=1.0f;
    _ListVw.layer.borderColor=[[UIColor blackColor]CGColor];
    _AdView.layer.borderWidth=1.0f;
    _AdView.layer.borderColor=[[UIColor blackColor]CGColor];
    self.RemlistTbl.separatorColor=[UIColor blackColor];
    self.RemlistTbl.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    RemlistTbl.dataSource=self;
    RemlistTbl.delegate=self;
    // Do any additional setup after loading the view.
    //hi
}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Reminder Listing";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *cellid=@"ReminderCell";
    ReminderListTableCell *cell=(ReminderListTableCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    
    [cell.contentView.layer setBorderColor:[UIColor blackColor].CGColor];
    [cell.contentView.layer setBorderWidth:1.0f];
    cell.RemListNmLbl.text=@"Subha";
    cell.RemListDtlbl.text=@"10.10.2015";
    cell.RemListReasonLbl.text=@"Birthday";
    cell.RemListBtn.layer.borderColor=[[UIColor blackColor]CGColor];
    cell.RemListBtn.layer.borderWidth=1.0f;
    [[cell.RemListBtn layer]setCornerRadius:4.0f];
    [cell.RemListBtn addTarget:self action:@selector(go:) forControlEvents:UIControlEventTouchUpInside];    return cell;
}
-(void)go:(UIButton*)sender
{
    [self performSegueWithIdentifier:@"Notification" sender:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
