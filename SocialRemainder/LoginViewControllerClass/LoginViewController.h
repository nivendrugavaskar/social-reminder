//
//  ViewController.h
//  SocialRemainder
//
//  Created by Samprita Roy on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property(strong,nonatomic)IBOutlet UITextField *emaillogin;
@property(strong,nonatomic)IBOutlet UITextField *passwordlogin;

@property (weak, nonatomic) IBOutlet UIButton *btnlogin;
@property (weak, nonatomic) IBOutlet UIButton *btnforgotpassword;
@property (weak, nonatomic) IBOutlet UIButton *btnsignup;

@property(weak,nonatomic)IBOutlet UIImageView *imgLogin;
@property(weak,nonatomic)NSString *UserId;


- (IBAction)btnloginclick:(id)sender;
- (IBAction)btnfpassclick:(id)sender;
- (IBAction)btnsignupclick:(id)sender;


//- (IBAction)Check:(id)sender;

@end

