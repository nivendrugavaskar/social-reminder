//
//  ViewController.m
//  SocialRemainder
//
//  Created by Samprita Roy on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/Login

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"
//#import "ModelClass.h"
NSString *const BaseUrl=@"http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/";

// NSString *const BaseUrl=@"http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/Login";


@interface LoginViewController ()


@end
int tag2,tag3;
NSString *alert;
@implementation LoginViewController
@synthesize emaillogin,passwordlogin,UserId;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.emaillogin.delegate=self;
    self.passwordlogin.delegate=self;
    [emaillogin becomeFirstResponder];
    self.navigationItem.title = @"Login";
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self getUUID];
    //[self WebServiceCall];
    // hi hello
    
    

}
- (NSString *)getUUID {
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceUUID"];
    if (string == nil) {
        CFUUIDRef   uuid;
        CFStringRef uuidStr;
        
        uuid = CFUUIDCreate(NULL);
        uuidStr = CFUUIDCreateString(NULL, uuid);
        
        string = [NSString stringWithFormat:@"%@", uuidStr];
        NSLog(@"Device id is:...%@",uuidStr);
        
        [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"deviceUUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        //        CFRelease(uuidStr);
        //        CFRelease(uuid);
    }
    
    return string;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Login";
    //[self WebServiceCall];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma samprita work

/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SignUp"]) {
        
        // Get destination view
        //RegisterViewController *rvc = [segue destinationViewController];
        
        // Get button tag number (or do whatever you need to do here, based on your object
        //NSInteger tagIndex = [(UIButton *)sender tag];
        
        // Pass the information to your destination view
        //[vc setSelectedButton:tagIndex];
    }
}*/


-(void)WebServiceCall
{
    NSString *stringUrl=[NSString stringWithFormat:@"%@Login",BaseUrl];
   
    AFHTTPRequestOperationManager *manager =[AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"UserLogin[username]": emaillogin.text,@"UserLogin[password]":passwordlogin.text,@"Profile[device_token]":@"@%",@"Profile[token_type]":@"iOS"};
    
    
    [manager POST:stringUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSDictionary *dict=[responseObject objectForKey:@"result"];
        NSLog(@"dic value is......%@",dict);
        
        
        NSDictionary *dict2=[dict objectForKey:@"status"];
        NSLog(@"dic value is......%@",dict2);
        // fetch user id
        
        NSDictionary *dictuser=[dict objectForKey:@"user"];
        NSLog(@"dicuser value is...%@",dictuser);
        UserId=[NSString stringWithFormat:@"%@",[dictuser objectForKey:@"userId"]];
        NSLog(@"USerid....%@",UserId);
        
        
        
        alert=[dict2 valueForKey:@"message"];
        
        NSDictionary *dictUseurId=[dict2 objectForKey:@"user"];
        
        NSLog(@"dic userid is....%@",dictUseurId);
        
        
        
        
        NSLog(@"Alert value is......%@",alert);
        
        
        tag3=1;  //Setting tag to check wheather WebService is Called or not...
        
        //Setting for Alert....
        if ([alert isEqualToString:@""])
        {
            NSDictionary *dict1=[NSDictionary alloc];
            
            dict1=[parameters objectForKey:@"response"];
            NSLog(@"Value.......%@",dict1);
            
        }
        //After Calling webservice call same button Action from where we call webservice.
        [_btnlogin setHighlighted:YES];
        [_btnlogin sendActionsForControlEvents:UIControlEventTouchUpInside];
        [_btnlogin performSelector:@selector(setHighlighted:) withObject:NO afterDelay:0.12];
        //[_btnlogin addTarget:self action:@selector(btnloginclick:) forControlEvents:UIControlEventAllEvents];
        
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         tag2=1;
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:[error localizedDescription]delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
- (IBAction)btnloginclick:(id)sender

{
  
    if(emaillogin.text.length<=0 || passwordlogin.text.length<=0)
    {
        UIAlertView *alert3=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Fill All Fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert3 show];
        [emaillogin becomeFirstResponder];
        
    }
    else if(passwordlogin.text.length<6)
    {
        UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Password Should Contain Six Characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert2 show];
        passwordlogin.text=@"";
        [passwordlogin becomeFirstResponder];
    }
    else
    {
        
        
        if(tag3==1)   //Checking WebService Response And Creating Alert..
        {
           if (![alert isEqualToString:@""])
           {
               UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Error" message:alert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert1 show];
               tag3=0;  //Setting For Calling WebService Everytime of Execution..
               if ([alert isEqualToString:@"Email is incorrect."])
               {
                 emaillogin.text=@"";
                 [emaillogin becomeFirstResponder];
               }
               else if ([alert isEqualToString:@"Password is incorrect."])
               {
                  passwordlogin.text=@"";
                  [passwordlogin becomeFirstResponder];
               }
            }
        
           else
           {
               
              if(tag2==0)
              {
                emaillogin.text=@"";
                passwordlogin.text=@"";
                [self performSegueWithIdentifier:@"DashBoard" sender:self];
              }
              else
              {
               UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Login Not Successful..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
              }
           }
        }
        else
        {
        [self WebServiceCall];
        }
    }
    
}

- (IBAction)btnfpassclick:(id)sender
{
    [self performSegueWithIdentifier:@"Forget" sender:self];
}

- (IBAction)btnsignupclick:(id)sender
{
    
    [self performSegueWithIdentifier:@"SignUp" sender:self];
    
}
/*-(void)CallingAction
{
[self WebServiceCall];
    UIButton *mybtn=[[UIButton alloc]init];
    [mybtn addTarget:self action:@selector(btnloginclick:)forState:UIControlEventTouchUpInside];
}
- (IBAction)Check:(id)sender
{
   [self performSegueWithIdentifier:@"Contact" sender:self]; 
}*/
@end
