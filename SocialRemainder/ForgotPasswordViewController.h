//
//  ForgotPasswordViewController.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtForgotEmail;
@property (strong, nonatomic) IBOutlet UILabel *ForgotPasslbl;
- (IBAction)btnForgotPass:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ForgotPassBtn;

@end
