//
//  NotificationViewController.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 23/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *BackVw;
@property (strong, nonatomic) IBOutlet UIView *FrntVw;
@property (strong, nonatomic) IBOutlet UIImageView *NotificationImgVw;
@property (strong, nonatomic) IBOutlet UILabel *NotificationLbl;
@property (strong, nonatomic) IBOutlet UILabel *NotifiInfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *NotifiDatetimeLbl;

@end
