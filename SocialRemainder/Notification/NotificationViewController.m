//
//  NotificationViewController.m
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 23/09/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import "NotificationViewController.h"

@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title=@"Notification";
    self.navigationController.navigationBar.topItem.title=@"Back";
    self.BackVw.layer.borderWidth=1.0f;
    self.BackVw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.FrntVw.layer.borderWidth=1.0f;
    self.FrntVw.layer.borderColor=[[UIColor blackColor]CGColor];
    self.NotificationImgVw.layer.borderWidth=1.0f;
    self.NotificationImgVw.layer.borderColor=[[UIColor redColor]CGColor];
    _NotificationLbl.text=@"You have 2 Notification set for Today.";
    [_NotificationLbl sizeToFit];
    _NotifiInfoLbl.text=@"Abhi,Ani,Arpi and some other have their birthday today... ";
    [_NotifiInfoLbl sizeToFit];
    _NotifiDatetimeLbl.text=@"23.09.2015 19:42pm";
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
self.navigationItem.title=@"Notification";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
