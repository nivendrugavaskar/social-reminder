//
//  DashBoardViewController.h
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 17/09/15.
//  Copyright (c) 2015 Samprita Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface DashBoardViewController : UIViewController<UIAlertViewDelegate>
@property(strong,nonatomic)IBOutlet UIView *RemVw;
@property(strong,nonatomic)IBOutlet UIImageView *RemImgVw;
@property (strong,nonatomic)IBOutlet UILabel *RemLbl;

- (IBAction)RemBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *CreateRemVw;
@property (strong, nonatomic) IBOutlet UILabel *CreateRemLbl;
@property (strong, nonatomic) IBOutlet UIImageView *CreateRemImgVw;

- (IBAction)CreateRemBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *AccSettVw;
@property (strong, nonatomic) IBOutlet UIImageView *AccSettImgVw;
@property (strong, nonatomic) IBOutlet UILabel *AccSettLbl;

- (IBAction)AccSettBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *RemSettVw;
@property (strong, nonatomic) IBOutlet UILabel *RemSettLbl;
@property (strong, nonatomic) IBOutlet UIImageView *RemSettImgVw;

- (IBAction)RemSettBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *LogOutVw;
@property (strong, nonatomic) IBOutlet UILabel *LogOutLbl;

- (IBAction)LogOutBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *LogOutImgVw;
# pragma Outlets of 5 Button 

@property (weak, nonatomic) IBOutlet UIButton *RemViewBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *CreateRemBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *RemSettBtnOutlet;

@property (weak, nonatomic) IBOutlet UIButton *AccSettBtnOutlet;


@property (weak, nonatomic) IBOutlet UIButton *LogoutBtnOutlet;
@end