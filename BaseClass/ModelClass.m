//
//  ModelClass.m
//  SocialRemainder
//
//  Created by Subhadeep Chakraborty on 01/10/15.
//  Copyright © 2015 Samprita Roy. All rights reserved.
//

#import "ModelClass.h"
//NSString const *BaseUrl1=@"http://dev1.businessprodemo.com/SocialReminder/php/index.php?r=Api/";
@implementation ModelClass


- (NSString *)getUUID
{
    [self getUUID];
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceUUID"];
    if (string == nil) {
        CFUUIDRef   uuid;
        CFStringRef uuidStr;
        
        uuid = CFUUIDCreate(NULL);
        uuidStr = CFUUIDCreateString(NULL, uuid);
        
        string = [NSString stringWithFormat:@"%@", uuidStr];
        NSLog(@"Device id is:...%@",uuidStr);
        
        [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"deviceUUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        //        CFRelease(uuidStr);
        //        CFRelease(uuid);
    }
    
    return string;
}
@end
